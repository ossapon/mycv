//
//  CurriculumController.swift
//  osapon-cv
//
//  Created by Alex Sapon on 5/1/19.
//  Copyright © 2019 Alex Sapon. All rights reserved.
//

let contacts = [
    "+380632613668",
    "Email",
    "Linked In",
    "Facebook"
]

import UIKit

class CurriculumViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate {
    
    // MARK - header image outlet
    @IBOutlet weak var headerImage: UIImageView!
    // MARK - profile image outlet
    @IBOutlet weak var profileImage: UIImageView!
    // MARK - scroll view outlet
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    // MARK - contact view outlet
    @IBOutlet weak var contactView: UIView!
    // MARK - contact links array
    @IBOutlet var contactTextViews: [UITextView]!
    // MARK - info labels view
    @IBOutlet weak var infoView: UIView!
    // MARK - variable for detecting scrolling direction
    var scrollDirection = 0
    // MARK - CV data labels
    @IBOutlet weak var expLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var eduLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        headerImage.layer.borderWidth = 1
        headerImage.layer.borderColor = UIColor.white.cgColor
        setRoundedImage()
        setContactLinks()
        descLabel.text = "  I'm 27 years old. Interested in software development. Started to learn programming about two years ago in UNIT Factory.\n  Since then I've made bunch of learning projects on C language, start my professional career as a Back-End developer and for the last two month have been learning Swift."
        expLabel.text = "  From May 2018 till now - Back-End Developer at AltRecipe company.\n  My main responsibilities are:\n   -WEB development\n   -Integration of external services\n   -REST API development\n   -Bug fixing"
        eduLabel.text = "  From Nov 2017 till now is a student of UNIT Factory\n\n  In marth 2016 get Master degree in accountment and audit in KNEU by V.Getman"
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print(URL)
        UIApplication.shared.open(URL)
        return false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var profileDelta = CGRect (
            x: profileImage.frame.origin.x,
            y: profileImage.frame.origin.y,
            width: profileImage.frame.width,
            height: profileImage.frame.height
        )
        var contactViewDelta = CGRect (
            x: contactView.frame.origin.x,
            y: contactView.frame.origin.y,
            width: contactView.frame.width,
            height: contactView.frame.height
        )
        var infoViewDelta = CGRect (
            x: infoView.frame.origin.x,
            y: infoView.frame.origin.y,
            width: infoView.frame.width,
            height: infoView.frame.height
        )
        
        var contactLinksDelta = contactTextViews.map {
            CGRect (
                x: $0.frame.origin.x,
                y: $0.frame.origin.y,
                width: $0.frame.width,
                height: $0.frame.height
            )
        }
        
        // get scrolling direction while continue pressing the screen
        let currentDirectionFlow =  scrollView.panGestureRecognizer.velocity(in: scrollView.superview).y
        let currentDirection = Int(currentDirectionFlow).signum()
        if currentDirection != self.scrollDirection &&
            currentDirection != 0 {
            self.scrollDirection = currentDirection
        }
        
        //changing position of items according to y offset
        let offset = scrollView.contentOffset.y
        if offset < 350 {
            if self.scrollDirection < 0 &&
                offset > 0 {
                profileDelta = getRect(widthPos: profileDelta.width > 90, heightPos: profileDelta.height > 90, coords: 0.9, size: -1.8, delta: profileDelta)
                contactViewDelta = getRect(widthPos: false, heightPos: profileDelta.height > 90, coords: -1.6, size: 0, delta: contactViewDelta)
                infoViewDelta = getRect(widthPos: false, heightPos: profileDelta.height > 90, coords: -10, size: 0, delta: infoViewDelta)
                contactLinksDelta = contactLinksDelta.enumerated().map {
                    getRect (
                        widthPos: false,
                        heightPos: profileDelta.height > 100,
                        coords: -(CGFloat($0)),
                        size: 0,
                        delta: $1)
                }
            } else if self.scrollDirection > 0 {
                profileDelta = getRect(widthPos: profileDelta.width < 150, heightPos: profileDelta.height < 150, coords: -0.9, size: 1.8, delta: profileDelta)
                contactViewDelta = getRect(widthPos: false, heightPos: profileDelta.height < 150, coords: 1.6, size: 0, delta: contactViewDelta)
                infoViewDelta = getRect(widthPos: false, heightPos: profileDelta.height < 150, coords: 10, size: 0, delta: infoViewDelta)
                contactLinksDelta = contactLinksDelta.enumerated().map {
                    getRect (
                        widthPos: false,
                        heightPos: profileDelta.height < 140,
                        coords: CGFloat($0),
                        size: 0,
                        delta: $1)
                }
            }
            
        }
        profileImage.frame = profileDelta
        contactView.frame = contactViewDelta
        infoView.frame = infoViewDelta
        contactTextViews.enumerated().forEach {
            $1.frame = contactLinksDelta[$0]
        }
        if offset > 20 {
            contactView.isHidden = self.scrollDirection < 0 ? true : false
        }
        setRoundedImage()
    }
    
    // get outlet coords
    func getRect(widthPos: Bool, heightPos: Bool, coords: CGFloat, size: CGFloat, delta: CGRect) -> CGRect {
        return CGRect (
            x: widthPos ? delta.minX + coords : delta.minX,
            y: heightPos ? delta.minY + coords : delta.minY,
            width: widthPos ? delta.width + size : delta.width,
            height: heightPos ? delta.height + size : delta.height
        )
    }
    
    // rounded profile picture
    func setRoundedImage() {
        profileImage.layer.borderWidth = 3
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
    }
    
    // taped contact
    func setContactLinks() {
        contactTextViews.enumerated().forEach {
            $1.isEditable = false
            $1.isSelectable = true
            $1.isScrollEnabled = false
            let attributedString = NSMutableAttributedString(string: contacts[$0])
            switch $1.textContentType.rawValue {
            case "tel":
                attributedString.addAttribute(.link, value: "tel:\($1.text ?? "")", range: NSRange(location: 0, length: contacts[$0].count ))
                break
            case "email":
                attributedString.addAttribute(.link, value: "mailto:\($1.text ?? "")", range: NSRange(location: 0, length: contacts[$0].count ))
                break
            default:
                attributedString.addAttribute(.link, value: "\($1.text ?? "")", range: NSRange(location: 0, length: contacts[$0].count ))
                break
            }
            $1.attributedText = attributedString
            $1.textAlignment = .center
            $1.font = UIFont(name: "Courier", size: 18)
        }
    }

}
