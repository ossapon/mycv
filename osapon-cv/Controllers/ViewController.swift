//
//  ViewController.swift
//  osapon-cv
//
//  Created by Alex Sapon on 4/12/19.
//  Copyright © 2019 Alex Sapon. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    // MARK scroll view outlet
    @IBOutlet weak var previewScrollView: UIScrollView! {
        didSet {
            previewScrollView.delegate = self;
        }
    }
    // MARK page controll outlet
    @IBOutlet weak var pageControll: UIPageControl!
    // MARK more button outlet
    @IBOutlet weak var moreButton: UIButton!
    
    // MARK amount of slides
    private var slidesAmount: Int  {
        guard let count = slides?.count else { return 0 }
        return count;
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSlides();
        previewScrollView.showsVerticalScrollIndicator = false
        setMoreButtonStyle()
        pageControll.numberOfPages = self.slidesAmount
        pageControll.currentPage = 0
        view.bringSubviewToFront(pageControll)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        loadSlides()
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.moreButton.isHidden = true
        
        // animate slides according to page index and swipe direction
        let pageIndex = round(previewScrollView.contentOffset.x/view.frame.width)
        pageControll.currentPage = Int(pageIndex)
        
        let maxHorizontalOffset: CGFloat = previewScrollView.contentSize.width - previewScrollView.frame.width
        let currentHorizontalOffset: CGFloat = previewScrollView.contentOffset.x
        
        let maxVerticalOffset: CGFloat = previewScrollView.contentSize.height - previewScrollView.frame.height
        let currentVerticalOffset: CGFloat = previewScrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maxHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maxVerticalOffset
        
        let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
        
        if percentOffset.x >= 0 &&
            percentOffset.x < 0.26 {
            slides?[0].slide.SlideImageView.transform = CGAffineTransform (
                scaleX: (0.25 - percentOffset.x) / 0.25,
                y: (0.25 - percentOffset.x) / 0.25
            )
            slides?[1].slide.SlideImageView.transform = CGAffineTransform (
                scaleX: percentOffset.x / 0.25,
                y: percentOffset.x / 0.25
            )
        } else if percentOffset.x > 0.25 &&
                    percentOffset.x < 0.51 {
            slides?[1].slide.SlideImageView.transform = CGAffineTransform (
                scaleX: (0.50 - percentOffset.x) / 0.25,
                y: (0.50 - percentOffset.x) / 0.25
            )
            slides?[2].slide.SlideImageView.transform = CGAffineTransform (
                scaleX: percentOffset.x / 0.50,
                y: percentOffset.x / 0.50
            )
        } else if percentOffset.x > 0.50 &&
                    percentOffset.x < 0.76 {
            slides?[2].slide.SlideImageView.transform = CGAffineTransform (
                scaleX: (0.75 - percentOffset.x) / 0.25,
                y: (0.75 - percentOffset.x) / 0.25
            )
            slides?[3].slide.SlideImageView.transform = CGAffineTransform (
                scaleX: percentOffset.x / 0.75,
                y: percentOffset.x / 0.75
            )
        } else if percentOffset.x > 0.75 &&
                    percentOffset.x <= 1 {
            slides?[3].slide.SlideImageView.transform = CGAffineTransform (
                scaleX: (1 - percentOffset.x) / 0.25,
                y: (1 - percentOffset.x) / 0.25
            )
            slides?[4].slide.SlideImageView.transform = CGAffineTransform (
                scaleX: percentOffset.x,
                y: percentOffset.x
            )
            self.moreButton.isHidden = false
        }
    }

    // pushing CV view
    @IBAction func moreAction(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "Curriculum") as! CurriculumViewController
        navigationController?.pushViewController(next, animated: true)
    }
    
    // more-button styles
    func setMoreButtonStyle() {
        self.moreButton.contentEdgeInsets = UIEdgeInsets(top: 3,left: 5,bottom: 3,right: 5)
        self.moreButton.layer.cornerRadius = 5
        self.moreButton.layer.borderWidth = 1
        self.moreButton.layer.borderColor = UIColor(red: 47, green: 181, blue: 40, alpha: 1).cgColor
        self.moreButton.showsTouchWhenHighlighted = true
        self.moreButton.isHidden = true
    }
    
    // loading slides
    func loadSlides() {
        previewScrollView.frame = CGRect (
            x: 0,
            y: 0,
            width: view.frame.width,
            height: view.frame.height
        )
        previewScrollView.contentSize = CGSize (
            width: view.frame.width * CGFloat(self.slidesAmount),
            height: view.frame.height - 20
        )
        previewScrollView.isPagingEnabled = true
        previewScrollView.bounces = false
        previewScrollView.alwaysBounceHorizontal = false
        
        slides?.enumerated().forEach {
            $1.slide.frame = CGRect (
                x: view.frame.width * CGFloat($0),
                y: 0,
                width: view.frame.width,
                height: view.frame.height
            )
            previewScrollView.addSubview($1.slide)
        }
    }
}

