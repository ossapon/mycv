//
//  Slide.swift
//  osapon-cv
//
//  Created by Alex Sapon on 4/12/19.
//  Copyright © 2019 Alex Sapon. All rights reserved.
//

import UIKit

class Slide: UIView {

    @IBOutlet weak var SlideImageView: UIImageView!
    @IBOutlet weak var SlideLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
