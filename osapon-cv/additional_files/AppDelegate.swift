//
//  AppDelegate.swift
//  osapon-cv
//
//  Created by Alex Sapon on 4/12/19.
//  Copyright © 2019 Alex Sapon. All rights reserved.
//

import UIKit

struct Slides {
    let slide: Slide
    
    init(slide: Slide, image: UIImage, text: String) {
        self.slide = slide
        self.slide.SlideImageView.image = image
        self.slide.SlideLabel.text = text
    }
}

let slidesData = [
    [0: UIImage(named: "kitty")!, 1: "Welcome to\nintroductory part\nof my CV app"],
    [0: UIImage(named: "kitty")!, 1: "The main goal\n of it is\nto introduce myself"],
    [0: UIImage(named: "kitty")!, 1: "..and apply for\n MacPaw Summer\nInternship"],
    [0: UIImage(named: "kitty")!, 1: "for get boost of my skills in\n MacOS software development."],
    [0: UIImage(named: "kitty")!, 1: "Hope you like this kitty:)\nPress \"more\""],
]

var slides: [Slides]?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        slides = slidesData.map{ Slides(slide: Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide, image: $0[0] as! UIImage, text: $0[1] as! String)}
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

